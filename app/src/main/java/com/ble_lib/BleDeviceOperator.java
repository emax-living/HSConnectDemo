package com.ble_lib;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.ble_lib.common.Constant;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import static android.bluetooth.BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;

/**
 * 蓝牙设备进行操作的Operator,包括Connect,Read,Write,Notify,
 */

public class BleDeviceOperator {
    private static final String TAG = BleDeviceOperator.class.getSimpleName();

    public static final int STATE_CONNECTED = BluetoothProfile.STATE_CONNECTED;
    public static final int STATE_DISCONNECTED = BluetoothProfile.STATE_DISCONNECTED;
    public static final int STATE_CONNECTING = BluetoothProfile.STATE_CONNECTING;


    private BluetoothDevice mBluetoothDevice;
    public BluetoothGatt mBluetoothGatt;
    private String mBluetoothDeviceAddress;
    private Context mContext;
    private BluetoothGattCallback mBluetoothGattCallback;
    private int mConnectState = STATE_DISCONNECTED;
    private Set<BleDeviceService> mOpeationService;
    private List<BluetoothGattService> mBluetoothGattService;


    /**
     * 蓝牙设备操作类
     *
     * @param mBluetoothDevice
     * @param mContext
     * @param mBluetoothCattCallback
     */
    public BleDeviceOperator(BluetoothDevice mBluetoothDevice,
                             Context mContext,
                             BluetoothGattCallback mBluetoothCattCallback) {
        this.mBluetoothDevice = mBluetoothDevice;
        this.mContext = mContext;
        this.mBluetoothGattCallback = mBluetoothCattCallback;
        initialize();

    }

    private void initialize() {
        if (mBluetoothDevice == null) {
            throw new IllegalArgumentException("BleDevice is null");
        }
        mBluetoothDeviceAddress = mBluetoothDevice.getAddress();
        mOpeationService = new HashSet<>();
    }

    /**
     * 连接这个蓝牙设备,得到Gatt
     */
    public void connectDevice() {
        mConnectState = STATE_CONNECTING;
        if (mBluetoothGatt != null) {
            mBluetoothGatt.close();
        }
        mBluetoothGatt = mBluetoothDevice.connectGatt(mContext, false, mBluetoothGattCallback);
        mBluetoothGatt.connect();
    }

    public void disconnectDevice() {
        try {
            mConnectState = STATE_DISCONNECTED;
            if (mBluetoothGatt == null) {
                return;
            }
            Log.w(TAG, "mBluetoothGatt closed");
            mBluetoothDeviceAddress = null;
            mBluetoothGatt.disconnect();
            mBluetoothGatt.close();
            mBluetoothGatt = null;
        }catch (Exception e){
            Log.e("Sadsadsad",e.getMessage());
        }

    }


    /**
     * 发现这个设备的服务
     */
    public void discoverDeviceServices() {
        if (isGattValid()) {
            mBluetoothGatt.discoverServices();
        }
    }


    /***
     * 蓝牙操作处理入口，分别分发到READ，NOTIFY，WRITE
     * @param bleDeviceService
     */
    public synchronized void processService(BleDeviceService bleDeviceService) {
        mOpeationService.add(bleDeviceService);
        if (mBluetoothGattService == null || mBluetoothGattService.size() == 0) {
            Log.e(TAG, "The service is not valid");
            return;
        }
        UUID characteristicUUID = bleDeviceService.getmCharacteristicUUID();
        BluetoothGattCharacteristic processCharacteristic = null;
        for (BluetoothGattService service : mBluetoothGattService) {
            if (service.getCharacteristic(characteristicUUID) == null) {
                continue;
            } else {
                processCharacteristic = service.getCharacteristic(characteristicUUID);
                break;
            }
        }

        if (isProcessValid(processCharacteristic, bleDeviceService)) {
            switch (bleDeviceService.getmOperationType()) {
                case Read:
                    readCharacteristic(processCharacteristic);
                    break;
                case Notify:
                case Indicate:
                    notifyCharacteristic(
                            processCharacteristic,
                            BleDeviceService.OperateType.Notify != bleDeviceService.getmOperationType());
                    break;
                case Write:
                    writeCharacteristic(processCharacteristic, bleDeviceService.getWriteData());
                    break;
            }
        }

    }

    private List<byte[]> dataArr;


    /**
     * Read操作
     *
     * @param characteristic
     */
    private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (isGattValid()) {
            if (mBluetoothGatt.setCharacteristicNotification(characteristic, true)) {
                BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Constant.NOTIFY_OR_INDICATE_DESCRIPTOR_UUID);
                if (descriptor != null) {
                    byte[] writingValue = ENABLE_NOTIFICATION_VALUE;
                    descriptor.setValue(writingValue);
                    mBluetoothGatt.readCharacteristic(characteristic);
                }
            }
        }
    }

    /**
     * Notify/Indicate操作
     *
     * @param characteristic
     */

    private void notifyCharacteristic(BluetoothGattCharacteristic characteristic, boolean isIndicate) {
        if (isGattValid()) {
            mBluetoothGatt.setCharacteristicNotification(characteristic, true);
            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(Constant.NOTIFY_OR_INDICATE_DESCRIPTOR_UUID);
            if (descriptor != null) {
                byte[] writingValue = isIndicate ? BluetoothGattDescriptor.ENABLE_INDICATION_VALUE : ENABLE_NOTIFICATION_VALUE;
                descriptor.setValue(writingValue);
                mBluetoothGatt.writeDescriptor(descriptor);
            } else {
                Log.e(TAG, characteristic.getUuid() + "上在找不到Config Descriptor");
            }
        }
    }

    /**
     * Write操作
     *
     * @param characteristic
     * @param writeData
     */
    private boolean writeCharacteristic(final BluetoothGattCharacteristic characteristic, final byte[] writeData) {
        if (isGattValid()) {
            Boolean isSuccess = false;
            characteristic.setValue(writeData);
            isSuccess = mBluetoothGatt.writeCharacteristic(characteristic);
            return isSuccess;
        }
        return false;
    }


    /**
     * 删除无用的service
     *
     * @param characteristic
     */
    public void deleteService(BluetoothGattCharacteristic characteristic) {
        for (int i = 0; i < mOpeationService.size(); i++) {
            BleDeviceService s = mOpeationService.iterator().next();
            if (s.getmCharacteristicUUID().equals(characteristic.getUuid())) {
                mOpeationService.remove(s);
            }
        }

    }


    /**
     * BleAdmin服务回调成功后调用，用来获得Service
     */
    public void setSerivce() {
        if (isGattValid()) {
            mBluetoothGattService = mBluetoothGatt.getServices();
        }
    }


    /**
     * 判定Process是否有效
     *
     * @param characteristic
     * @param service
     * @return
     */
    private boolean isProcessValid(BluetoothGattCharacteristic characteristic, BleDeviceService service) {
        if (characteristic == null) {
            Toast.makeText(mContext, "the charatsic is not valid", Toast.LENGTH_SHORT).show();
            return false;
        }
        int charaProp = characteristic.getProperties();
        switch (service.getmOperationType()) {
            case Read:
                return (charaProp & BluetoothGattCharacteristic.PROPERTY_READ) != 0;
            case Notify:
                return (charaProp & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0;
            case Write:
                return (characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0;
        }
        Toast.makeText(mContext, "操作类型不支持charachteristic", Toast.LENGTH_SHORT).show();
        return false;
    }

    /**
     * 校验gatt是否存在
     *
     * @return
     */
    private boolean isGattValid() {
        if (mBluetoothGatt == null) {
            throw new IllegalArgumentException("The Gatt is unvalid");
        }
        return true;
    }


}
