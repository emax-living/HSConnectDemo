package com.ble_lib;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;


import com.ble_lib.callback.DeviceConnectStateCallback;
import com.ble_lib.callback.DeviceOperationCallback;
import com.ble_lib.callback.ScanCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Bluetooth的总类，ble设备管理，扫描.
 */

public class BleAdmin extends android.bluetooth.le.ScanCallback {
    private static final String TAG = BleAdmin.class.getSimpleName();


    private Context mContext;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private int mDeviceCount;//搜索到的蓝牙设备的数量
    private int connectCount = 0;


    private ScanCallback mScanCallBack;
    private List<BluetoothDevice> mScanDevices;

    private Map<String, BleDeviceOperator> mConnectedDevice;
    private Map<String, BleDeviceOperator> mConnectingDevice;

    private DeviceConnectStateCallback mDeviceCallback;
    private DeviceOperationCallback mDeviceOperationCallback;
    private BluetoothLeScanner scanner;
    private BleDeviceOperator deviceOperator;

    public BleAdmin(Context mContext, DeviceConnectStateCallback deviceConnectCallback, DeviceOperationCallback mDeviceOperationCallback) {
        this.mContext = mContext;
        this.mDeviceCallback = deviceConnectCallback;
        this.mDeviceOperationCallback = mDeviceOperationCallback;
        initialize();
    }

    public BleAdmin(Context mContext) {
        this.mContext = mContext;
        initialize();
    }

    public Map<String, BleDeviceOperator> getmConnectingDevice() {
        return mConnectingDevice;
    }

    /**
     * 初始化蓝牙一些操作
     */
    private void initialize() {
        mBluetoothManager = (BluetoothManager) mContext.getSystemService(Context.BLUETOOTH_SERVICE);
        if (mBluetoothManager != null) {
            mBluetoothAdapter = mBluetoothManager.getAdapter();
            scanner = mBluetoothAdapter.getBluetoothLeScanner();
        }
        mConnectedDevice = new HashMap<>();
        if (mBluetoothAdapter == null) {
            throw new IllegalArgumentException("Your device is not support ble");
        }
    }

    /**
     * 判断蓝牙是否打开
     *
     * @return
     */
    public boolean isBleEnable() {
        return mBluetoothAdapter.isEnabled();
    }

    /**
     * 判断蓝牙是连接设备了
     *
     * @return
     */
    public boolean isBleConnect() {
        if (isBleEnable()) {
            int a2dp = mBluetoothAdapter.getProfileConnectionState(BluetoothProfile.A2DP);              //可操控蓝牙设备，如带播放暂停功能的蓝牙耳机
            int headset = mBluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEADSET);        //蓝牙头戴式耳机，支持语音输入输出
            int health = mBluetoothAdapter.getProfileConnectionState(BluetoothProfile.HEALTH);
            //查看是否蓝牙是否连接到三种设备的一种，以此来判断是否处于连接状态还是打开并没有连接的状态
            if (a2dp == BluetoothProfile.STATE_CONNECTED) {
                return true;
            } else if (headset == BluetoothProfile.STATE_CONNECTED) {
                return false;
            } else if (health == BluetoothProfile.STATE_CONNECTED) {
                return false;
            }
        }
        return false;
    }

    /**
     * open ble
     */
    public Boolean openBle() {
        return !isBleEnable() && mBluetoothAdapter.enable();
    }


    /**
     * disable ble
     */
    public Boolean disableBle() {
        return isBleEnable() && mBluetoothAdapter.disable();
    }


    public void startFilterScan(List<ScanFilter> scanFilters, ScanSettings scanSettings, ScanCallback mScanCallBack) {
        this.mScanCallBack = mScanCallBack;
        //初始化mScanDevices
        mDeviceCount = 0;
        if (mScanDevices == null) {
            mScanDevices = new ArrayList<>();
        }
        mScanDevices.clear();
        if (scanner != null) {
            if (scanFilters == null || scanSettings == null) {
                scanner.startScan(this);
                return;
            }
            scanner.startScan(scanFilters, scanSettings, this);
        }
    }

    /**
     * 取消查找
     */
    public void stopScan() {
        if (scanner != null) {
            scanner.stopScan(this);
        }
    }


    @Override
    public void onScanResult(int callbackType, ScanResult result) {
        if (result.getDevice() == null) {
            return;
        }
        if (TextUtils.isEmpty(result.getDevice().getName())) {
            return;
        }
//        Log.v(TAG, "macId:" + result.getDevice().getAddress() + "-" + result.getScanRecord().getDeviceName() + "");//lc change
        mScanDevices.add(result.getDevice());
        mScanCallBack.onDeviceFound(result.getDevice(), result.getScanRecord());
        connectCount = 0;
        super.onScanResult(callbackType, result);
    }

    @Override
    public void onScanFailed(int errorCode) {
        super.onScanFailed(errorCode);
        if (errorCode == 1) {
            return;
        }
        Log.e(TAG, "ble扫描失败错误码:" + errorCode + "");
        mScanCallBack.onFoundFail();
    }

    /**
     * 通过地址直接连接设备
     *
     * @param deviceAddress
     */
    public void connectDevice(String deviceAddress) {
        if (mConnectingDevice != null && mConnectingDevice.containsKey(deviceAddress)) {
            return;
        }
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(deviceAddress);
        connectDevice(device);
    }

    /**
     * 通过设备直接连接
     *
     * @param device
     */
    public synchronized void connectDevice(final BluetoothDevice device) {
        connectCount = 0;
        deviceOperator = new BleDeviceOperator(device, mContext, mBluetoothCattCallback);
        deviceOperator.connectDevice();
        if (mConnectingDevice == null) {
            mConnectingDevice = new HashMap<>();
        }
        if (!mConnectingDevice.containsKey(device.getAddress())) {
            mConnectingDevice.put(device.getAddress(), deviceOperator);
        }
    }

    //判断是否连接的是当前设备
    public Boolean isConnectCurrentMac(String address) {
        if (deviceOperator == null) {
            closeAllBleConnect();
            return false;
        } else if (deviceOperator.mBluetoothGatt != null && deviceOperator.mBluetoothGatt.getDevice().equals(address)) {
            connectedDevice(address);
            return true;
        }
        return false;
    }

    private void connectedDevice(String address) {
        if (mConnectedDevice == null) {
            mConnectedDevice = new HashMap<>();
        }
        mConnectedDevice.putAll(mConnectingDevice);
        discoverDeviceServices(address);
    }

    public void disconnectDevice(String address) {
        if (mConnectedDevice != null && mConnectedDevice.containsKey(address)) {
            mConnectedDevice.get(address).disconnectDevice();
            mConnectedDevice.remove(address);
        }
        if (mConnectingDevice != null && mConnectingDevice.containsKey(address)) {
            mConnectingDevice.get(address).disconnectDevice();
            mConnectingDevice.remove(address);
        }
    }

    public void closeAllBleConnect() {
        Iterator<Map.Entry<String, BleDeviceOperator>> it = mConnectedDevice.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, BleDeviceOperator> entry = it.next();
            entry.getValue().disconnectDevice();
            it.remove();
        }
        if (mConnectingDevice != null) {
            Iterator<Map.Entry<String, BleDeviceOperator>> it2 = mConnectingDevice.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry<String, BleDeviceOperator> entry = it2.next();
                entry.getValue().disconnectDevice();
                it2.remove();
            }
        }
    }

    public void disconnectDevice(BluetoothDevice device) {
        disconnectDevice(device.getAddress());
    }

    /**
     * 发现服务
     *
     * @param address
     */
    public void discoverDeviceServices(String address) {
        if (mConnectedDevice != null && mConnectedDevice.containsKey(address)) {
            mConnectedDevice.get(address).discoverDeviceServices();
        }
    }

//    /**
//     * 开启读特征
//     *
//     * @param address
//     * @param characteristic
//     */
//    public void readDeviceService(String address, BluetoothGattCharacteristic characteristic) {
//        if (!isReadCharacteristic(characteristic)) {
//            throw new IllegalArgumentException("The characteris is not a read charachteristic");
//        }
//        if (mConnectedDevice != null && mConnectedDevice.containsKey(address)) {
//            mConnectedDevice.get(address).
//        }
//    }

    /**
     * 蓝牙操作服务，
     *
     * @param service
     */
    public void processDeviceService(BleDeviceService service) {
        if (mConnectedDevice != null && mConnectedDevice.containsKey(service.getmDeviceAddress())) {
            mConnectedDevice.get(service.getmDeviceAddress()).processService(service);
        }
    }


    /**
     * 基础回调
     */
    //蓝牙连接状态
    public static final int STATE_CONNECTED = BluetoothProfile.STATE_CONNECTED;
    public static final int STATE_DISCONNECTED = BluetoothProfile.STATE_DISCONNECTED;
    public static final int STATE_CONNECTING = BluetoothProfile.STATE_CONNECTING;
    private BluetoothGattCallback mBluetoothCattCallback = new BluetoothGattCallback() {
        //连接成功后，还需要发现服务成功后才能进行相关操作

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            String address = gatt.getDevice().getAddress();
            if (newState == STATE_CONNECTED) {
                Log.e(TAG, " connect state is " + newState);
                if (!mConnectedDevice.containsKey(address)) {
                    mConnectedDevice.put(address, mConnectingDevice.get(address));
                    mConnectingDevice.remove(address);
                }
                mDeviceCallback.onDeviceConnected(address);
                connectCount = 0;
                deviceOperator = null;
            } else if (newState == STATE_DISCONNECTED) {
                if (status == 0) {
                    gatt.connect();
                } else {
                    connectCount++;
                    if (connectCount < 3) {
                        if (deviceOperator != null) {
                            deviceOperator.connectDevice();
                        }
                        Log.e(TAG, " connect state is fail,status:" + status + "newState:" + newState);
                    } else {
                        if (mConnectedDevice.containsKey(address)) {
                            mConnectedDevice.remove(address);
                        }
                        if (mConnectingDevice.containsKey(address)) {
                            mConnectingDevice.get(address).disconnectDevice();
                            mConnectingDevice.remove(address);
                        }
                        mDeviceCallback.onDeviceDisconnected();
                    }
                }
            }
        }

        //服务发现成功后，我们就可以进行数据相关的操作了，比如写入数据、开启notify等等
        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            String address = gatt.getDevice().getAddress();
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mConnectedDevice.get(address).setSerivce();
                mDeviceOperationCallback.onDeviceServiceDiscover(address, gatt.getServices(), gatt);
            }
        }


        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicRead(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                mConnectedDevice.get(gatt.getDevice().getAddress()).deleteService(characteristic);
                mDeviceOperationCallback.onDeviceCharacteristicRead(gatt.getDevice().getAddress(), characteristic);
            } else {
                Log.e(TAG, " write failure");
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.e(TAG, " write success");
                mDeviceOperationCallback.onDeviceCharacteristicWrite(gatt.getDevice().getAddress());
            } else {
                Log.e(TAG, " write failure");
                mDeviceOperationCallback.CharacteristicWriteFail();
            }

        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            String address = gatt.getDevice().getAddress();
            Log.e(TAG, " ++++++++++++++++++++++++++++++++++++++++++++++++++success" + address);
            mDeviceOperationCallback.onDeviceCharacteristicNotify(address, characteristic);
        }
    };


}
