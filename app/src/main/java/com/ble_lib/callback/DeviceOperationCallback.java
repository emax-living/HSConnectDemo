package com.ble_lib.callback;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;

import java.util.List;


public interface DeviceOperationCallback {
    /**
     * 发现BLE设备的服务的回调
     *
     * @param deviceAddress
     * @param services
     */
    void onDeviceServiceDiscover(String deviceAddress, List<BluetoothGattService> services, BluetoothGatt gatt);

    /**
     * 对一个BLE设备进行READ操作的回调
     *
     * @param deviceAddress
     * @param characteristic
     */
    void onDeviceCharacteristicRead(String deviceAddress, BluetoothGattCharacteristic characteristic);

    /**
     * 对一个BLE设备进行WRITE操作的回调
     *
     * @param deviceAddress
     */
    void onDeviceCharacteristicWrite(String deviceAddress);

    /**
     * 对一个BLE设备进行NOTIFY的回调
     *
     * @param deviceAddress
     * @param characteristic
     */
    void onDeviceCharacteristicNotify(String deviceAddress, BluetoothGattCharacteristic characteristic);

    /**
     * 指令写入失败
     */
    void CharacteristicWriteFail();
}
