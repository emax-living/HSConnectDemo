package com.other_utils.Bean;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Created by 90835 on 2017/10/12.
 */
public class DeviceInfo implements Parcelable {
    private String id;
    private int eType;//device location type，0: indoor，1:outdoor
    private String pType;//device location name
    private int imageId;
    private String ModelName;
    private String LocalName;
    private String imagePath;
    private int custom;//0:custom，1:system default
    //pairing time
    private long bindTime;
    //support type
    private String typesStr;

    private long setDeviceTime;

    private boolean isSetTime;

    public boolean isSetTime() {
        return isSetTime;
    }

    public void setSetTime(boolean setTime) {
        isSetTime = setTime;
    }

    public long getBindTime() {
        return bindTime;
    }

    public void setBindTime(long bindTime) {
        this.bindTime = bindTime;
    }

    public String getModelName() {
        return ModelName;
    }

    public void setModelName(String modelName) {
        ModelName = modelName;
    }

    public String getTypesStr() {
        return typesStr;
    }

    public void setTypesStr(String typesStr) {
        this.typesStr = typesStr;
    }

    public String getStringType(Context context) {
        switch (eType) {
            case 0:
                return "";
            case 1:
                return "outdoor";
        }
        return "device_place_default";
    }

    public String setStringType(String type, Context context) {
        if (("device_place_default").equals(type)) {
            seteType(0);
        } else if (("outdoor").equals(type)) {
            seteType(1);
        }
        return "device_place_default";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int geteType() {
        return eType;
    }

    public void seteType(int eType) {
        this.eType = eType;
    }

    public String getpType() {
        return pType;
    }

    public void setpType(String pType) {
        this.pType = pType;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public int getEType() {
        return this.eType;
    }

    public void setEType(int eType) {
        this.eType = eType;
    }

    public String getPType() {
        return this.pType;
    }

    public void setPType(String pType) {
        this.pType = pType;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getCustom() {
        return custom;
    }

    public void setCustom(int custom) {
        this.custom = custom;
    }

    public long getSetDeviceTime() {
        return setDeviceTime;
    }

    public void setSetDeviceTime(long setDeviceTime) {
        this.setDeviceTime = setDeviceTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DeviceInfo)) return false;

        DeviceInfo that = (DeviceInfo) o;

        return id.equals(that.id);
    }

    public String getLocalName() {
        return this.LocalName;
    }

    public void setLocalName(String LocalName) {
        this.LocalName = LocalName;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeInt(this.eType);
        dest.writeString(this.pType);
        dest.writeInt(this.imageId);
        dest.writeString(this.ModelName);
        dest.writeString(this.LocalName);
        dest.writeString(this.imagePath);
        dest.writeInt(this.custom);
        dest.writeLong(this.bindTime);
        dest.writeString(this.typesStr);
        dest.writeLong(this.setDeviceTime);
        dest.writeByte(this.isSetTime ? (byte) 1 : (byte) 0);
//        dest.writeParcelable(this.place, flags);
    }

    public DeviceInfo() {
    }

    protected DeviceInfo(Parcel in) {
        this.id = in.readString();
        this.eType = in.readInt();
        this.pType = in.readString();
        this.imageId = in.readInt();
        this.ModelName = in.readString();
        this.LocalName = in.readString();
        this.imagePath = in.readString();
        this.custom = in.readInt();
        this.bindTime = in.readLong();
        this.typesStr = in.readString();
        this.setDeviceTime = in.readLong();
        this.isSetTime = in.readByte() != 0;
//        this.place = in.readParcelable(Place.class.getClassLoader());
    }

    public DeviceInfo(String id, int eType, String pType, int imageId,
                      String ModelName, String LocalName, String imagePath, int custom,
                      long bindTime, String typesStr, long setDeviceTime) {
        this.id = id;
        this.eType = eType;
        this.pType = pType;
        this.imageId = imageId;
        this.ModelName = ModelName;
        this.LocalName = LocalName;
        this.imagePath = imagePath;
        this.custom = custom;
        this.bindTime = bindTime;
        this.typesStr = typesStr;
        this.setDeviceTime = setDeviceTime;
    }

    public static final Creator<DeviceInfo> CREATOR = new Creator<DeviceInfo>() {
        @Override
        public DeviceInfo createFromParcel(Parcel source) {
            return new DeviceInfo(source);
        }

        @Override
        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };
}
