package com.other_utils.Bean;

/**
 * Created by 90835 on 2017/11/14.
 */

public class RxPageStatusData {

    //实时数据命令
    public static final int GET_REATAIL_TIME_DATA = 0;
    //绑定扫描
    public static final int BIND_SCAN_DEVICE = GET_REATAIL_TIME_DATA + 1;
    //绑定设备更新ID
    public static final int UPDATE_DEVICE_ID = BIND_SCAN_DEVICE + 1;
    //历史数据获取
    public static final int GET_HISTORY = UPDATE_DEVICE_ID + 1;
    //终止历史数据获取
    public static final int STOP_HISTORY = GET_HISTORY + 1;
    //授时
    public static final int SET_DEVICE_TIME = STOP_HISTORY + 1;
    //关于设置连接
    public static final int SET_CONNECT = SET_DEVICE_TIME + 1;

    //设置警报值
    public static final int SET_ALARM_VALUE = SET_CONNECT + 1;

    //退出应用关闭所有ble缓存
    public static final int CLOSE_BLE = SET_ALARM_VALUE + 1;

    private int status;

    private String address;

    private HistoryLastInfo historyLastInfo;

    private AlarmData alarmData;

//    private ScanEvent.BleOperationCallBack callBack;
//
//    public HistoryLastInfo getHistoryLastInfo() {
//        return historyLastInfo;
//    }
//
//    public void setHistoryLastInfo(HistoryLastInfo historyLastInfo) {
//        this.historyLastInfo = historyLastInfo;
//    }
//
//    public AlarmData getAlarmData() {
//        return alarmData;
//    }
//
//    public void setAlarmData(AlarmData alarmData) {
//        this.alarmData = alarmData;
//    }
//
//    public ScanEvent.BleOperationCallBack getCallBack() {
//        return callBack;
//    }
//
//    public void setCallBack(ScanEvent.BleOperationCallBack callBack) {
//        this.callBack = callBack;
//    }
//
//    public int getStatus() {
//        return status;
//    }
//
//    public void setStatus(int status) {
//        this.status = status;
//    }
//
//    public String getAddress() {
//        return address;
//    }
//
//    public void setAddress(String address) {
//        this.address = address;
//    }
}
