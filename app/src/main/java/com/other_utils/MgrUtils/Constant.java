package com.other_utils.MgrUtils;

import android.os.Environment;

import java.io.File;

/**
 * Created by 90835 on 2017/10/9.
 */

public class Constant {
    public final static String DIR = Environment.getExternalStorageDirectory() + File.separator + "com.emax.house";

    /*设备型号名称*/

    public static final String TX_10 = "Tx0";

    /*设备serviceId*/
    public static String mServiceUuids="00001812-0000-1000-8000-00805f9b34fb";

    /*头部*/
    public static final int HEADER_IS = 0xCD;
    /*型号*/
    public static final int MODEL_IS = 0x55;
    /*编号*/
    public static final int CONTROL_NO = 0x01;
    /*应答*/
    public static final int REPLY = 0x01;
    /*消息尾*/
    public static final int TAIL = 0xDB;
    /*控制类一级指令*/
    public static final int C_LEVEL_IS = 0x43;
    /*数据类一级指令*/
    public static final int V_SECONDARY_IS = 0x56;
    /*设置类一级指令*/
    public static final int S_LEVEL_IS = 0x53;

    /*
    * 控制类二级
    *
    * */
    /*背光控制*/
    public static final int lights_On = 0x01;
    /*声音控制*/
    public static final int voice_control = 0x02;
    /*关机控制*/
    public static final int shutdown_control = 0x03;
    /*开机控制*/
    public static final int boot_control = 0x04;
    /*单位*/
    public static final int unit_control = 0x05;

    /*
  * 数据类二级
  *
  * */
    /*设备上报数据*/
    public static final int device_up_data = 0x01;
    /*服务端或者APP端下传数据*/
    public static final int device_get_data = 0x02;
    /*答应对应信息*/
    public static final int back_info = 0x03;
    /*请求历史数据*/
    public static final int get_history_data = 0x04;
    /*接收非法数据*/
    public static final int get_error_data = 0x05;

    /*
* 设置类二级
*
* */
    /*
    * 登录ID
    * */
    public static final int login_id_is = 0x0A;

    /*单点温度报警值设置*/
    public static final int temperature_alarm = 0x01;
    /*单点湿度报警值设置*/
    public static final int humidity_alarm = 0x02;
    /*单点气压报警值*/
    public static final int pressure_alarm = 0x03;

    /*风量报警值*/
    public static final int CFM_alarm = 0x04;
    /*设备ID*/
    public static final int device_id = 0x0A;
    /*响闹时间设置*/
    public static final int clock_remind_id = 0x0b;
    /*雨量报警值*/
    public static final int rainfall_remind_id = 0x05;
    /*向设备授时*/
    public static final int set_time_device_id = 0x06;
    /*带上下限温度报警值设置*/
    public static final int double_temperature_alarm = 0x07;
    /*带上下限湿度报警值设置*/
    public static final int double_humidity_alarm = 0x08;



    /*数据类以及设置类编码格式*/

    /*历史数据初始地址*/
    public static final int last_address = 0x70;


/*    温度上限报警：Local Name  scan_resp_data[11]=0x01  表示温度上限报警
    温度下限报警：Local Name  scan_resp_data[11]=0x02  表示温度下限报警
    湿度上限报警：Local Name  scan_resp_data[11]=0x04  表示湿度上限报警
    湿度下限报警：Local Name  scan_resp_data[11]=0x08  表示湿度下限报警*/

    public static final byte temperatureMaxAlarm = 0x31;
    public static final byte temperatureMinAlarm = 0x32;
    public static final byte humidityMaxAlarm = 0x34;
    public static final byte humidityMinAlarm = 0x38;

}
